/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *******************************************************************************/
/**
 * Created by Kevin Moran on May 25, 2016
 */
package edu.semeru.android.mutation.pit;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.semeru.android.mutation.helper.TerminalHelper;
import edu.semeru.android.mutation.service.ExtractDataService;


/**
 * @author KevinMoran
 *
 */
public class PITMutationExecutorThread extends Thread{

	private String baseOutputFolder; 
	private String baseInputFolder;
	private String projectName;
	private String mutantProjectsFolder;
	private String mutantAPKsPath; 
	private String baseProjectsFolder;
	private int mutantNumber; 
	private int workerNum;
	
	/**
	 * @param baseOutputFolder
	 * @param baseInputFolder
	 * @param projectName
	 * @param mutantProjectsFolder
	 * @param mutantAPKsPath
	 * @param baseProjectsFolder
	 */
	public PITMutationExecutorThread(String baseOutputFolder, String baseInputFolder, String projectName,
			String mutantProjectsFolder, String mutantAPKsPath, String baseProjectsFolder, int mutantNumber, int workerNum) {
		super();
		this.baseOutputFolder = baseOutputFolder;
		this.baseInputFolder = baseInputFolder;
		this.projectName = projectName;
		this.mutantProjectsFolder = mutantProjectsFolder;
		this.mutantAPKsPath = mutantAPKsPath;
		this.baseProjectsFolder = baseProjectsFolder;
		this.mutantNumber = mutantNumber;
		this.workerNum = workerNum;
	}
	
	@Override
	public void run() {
	
		String copyCommand = "";
		String createDirCommand = "";
		String createDirCommand2 = "";
		String findCommand = "";
		String findResult = "";

		Path projectOutputPath = Paths.get(mutantAPKsPath + File.separator + projectName + File.separator);

			//System.out.println("-- Assembling Mutant #:" + mutantNumber + "...");
			
			projectOutputPath = Paths.get(mutantProjectsFolder + File.separator + projectName + File.separator
					+ "mutant-" + mutantNumber + File.separator);

			if (!Files.exists(projectOutputPath)) {

				createDirCommand = "mkdir " + mutantProjectsFolder + File.separator + projectName + File.separator;
				createDirCommand2 = "mkdir " + mutantProjectsFolder + File.separator + projectName + File.separator
						+ "mutant-" + mutantNumber + File.separator;
//				System.out.println(createDirCommand);
//				System.out.println(createDirCommand2);
//				System.err.println(TerminalHelper.executeCommand(createDirCommand));
//				System.err.println(TerminalHelper.executeCommand(createDirCommand2));
				TerminalHelper.executeCommand(createDirCommand);
				TerminalHelper.executeCommand(createDirCommand2);
			}

			copyCommand = "cp -R " + baseInputFolder + File.separator + projectName + File.separator + "classes/ "
					+ mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + mutantNumber
					+ File.separator;
//			System.out.println(copyCommand);
//			System.err.println(TerminalHelper.executeCommand(copyCommand));
			TerminalHelper.executeCommand(copyCommand);

			findCommand = "find " + baseOutputFolder + File.separator + projectName + File.separator + "bytecode"
					+ File.separator + "mutant-" + mutantNumber + File.separator + " -name *.class";
			findResult = TerminalHelper.executeCommand(findCommand);
			
			findResult = findResult.substring(findResult.lastIndexOf("/")+1, findResult.length());
			
			findCommand = "find " + mutantProjectsFolder + File.separator
					+ projectName + File.separator + "mutant-" + mutantNumber + File.separator + " -name " + findResult;
			findResult = TerminalHelper.executeCommand(findCommand);
			
			PITMutationExecutor.removeFile(findResult);
			
			copyCommand = "cp -R " + baseOutputFolder + File.separator + projectName + File.separator + "bytecode"
					+ File.separator + "mutant-" + mutantNumber + File.separator + " " + mutantProjectsFolder + File.separator
					+ projectName + File.separator + "mutant-" + mutantNumber + File.separator;
//			System.out.println(copyCommand);
//			System.err.println(TerminalHelper.executeCommand(copyCommand));
			TerminalHelper.executeCommand(copyCommand);

			copyCommand = "cp -R " + baseProjectsFolder + File.separator + projectName + File.separator + " "
					+ mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + mutantNumber + "-project"
					+ File.separator;
//			System.out.println(copyCommand);
//			System.err.println(TerminalHelper.executeCommand(copyCommand));
			TerminalHelper.executeCommand(copyCommand);

			ExtractDataService.compileClassesToJar(
					mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + mutantNumber
							+ File.separator,
					mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + mutantNumber + ".jar ");
			ExtractDataService.compileJarToDex(
					mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + mutantNumber + ".jar",
					mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + mutantNumber + ".dex");

			copyCommand = "cp -R " + mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-"
					+ mutantNumber + ".dex " + mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + mutantNumber
					+ "-project" + File.separator + "classes.dex";
//			System.out.println(copyCommand);
//			System.out.println(TerminalHelper.executeCommand(copyCommand));
			TerminalHelper.executeCommand(copyCommand);
			ExtractDataService.compileDexToAPK(
					mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + mutantNumber + "-project",
					mutantAPKsPath + File.separator + projectName + File.separator + mutantNumber + ".apk");
			ExtractDataService
					.signAPKwithDebugSig(mutantAPKsPath + File.separator + projectName + File.separator + mutantNumber + ".apk");
			//System.out.println();

		MutantWorkerQueue.getInstance().add(workerNum);

	}
	
	
}
