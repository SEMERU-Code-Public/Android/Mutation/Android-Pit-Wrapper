/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *******************************************************************************/
/**
 * Created by Kevin Moran on May 25, 2016
 */
package edu.semeru.android.mutation.pit;

import java.util.ArrayList;
import java.util.Collection;

import org.pitest.classinfo.ClassName;
import org.pitest.mutationtest.engine.Mutant;
import org.pitest.mutationtest.engine.MutationDetails;

/**
 * @author KevinMoran
 *
 */
public class PITMutationWrapperThread extends Thread {
	
	private boolean finishedFlag;
	String className;
	PITMutationWrapper wrapper;
	ArrayList<Mutant> threadMutations;
	
	
	/**
	 * 
	 */
	public PITMutationWrapperThread(String className, PITMutationWrapper wrapper) {

		this.className = className;
		this.wrapper = wrapper;
	
	}
	@Override
	public void run(){
		
		finishedFlag = false;
		
		threadMutations = new ArrayList<>();
		
		try {
			threadMutations = mutateClass(className, wrapper);
		} catch (Exception e) {
			finishedFlag = true;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finishedFlag = true;
		
	}

	private ArrayList<Mutant> mutateClass(String className, PITMutationWrapper wrapper) throws Exception {

        Class<?> classToLoad = Class.forName(className, true, wrapper.urlLoader);

        ClassName toBeMutated = new ClassName(classToLoad.getCanonicalName());
        Collection<MutationDetails> details = wrapper.mutater.findMutations(toBeMutated);
        //Iterator<MutationDetails> iterator = details.iterator();
        ArrayList<Mutant> mutants = new ArrayList<Mutant>();
        for (MutationDetails detail : details) {

            Mutant mutant = wrapper.mutater.getMutation(detail.getId());
            mutants.add(mutant);
        }

        return mutants;
    }
	
	/**
	 * @return the finishedFlag
	 */
	public boolean isFinishedFlag() {
		return finishedFlag;
	}
	/**
	 * @param finishedFlag the finishedFlag to set
	 */
	public void setFinishedFlag(boolean finishedFlag) {
		this.finishedFlag = finishedFlag;
	}
	/**
	 * @return the wrapper
	 */
	public PITMutationWrapper getWrapper() {
		return wrapper;
	}
	/**
	 * @param wrapper the wrapper to set
	 */
	public void setWrapper(PITMutationWrapper wrapper) {
		this.wrapper = wrapper;
	}
	/**
	 * @return the threadMutations
	 */
	public ArrayList<Mutant> getThreadMutations() {
		return threadMutations;
	}
	/**
	 * @param threadMutations the threadMutations to set
	 */
	public void setThreadMutations(ArrayList<Mutant> threadMutations) {
		this.threadMutations = threadMutations;
	}
	
}
