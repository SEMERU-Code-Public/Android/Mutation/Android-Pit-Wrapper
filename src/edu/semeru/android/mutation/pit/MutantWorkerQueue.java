/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *******************************************************************************/
/**

 * Created by Kevin Moran on May 25, 2016
 */
package edu.semeru.android.mutation.pit;

import java.util.LinkedList;
import java.util.Queue;


/**
 * @author KevinMoran
 *
 */
public class MutantWorkerQueue {

		private static MutantWorkerQueue instance = null;
		private static Queue<Integer> mutantWorkerQueue;
		
		private MutantWorkerQueue() {
			mutantWorkerQueue = new LinkedList<Integer>();

		}
		public static synchronized MutantWorkerQueue getInstance() {
			if(instance == null) {
				instance = new MutantWorkerQueue();
			}
			return instance;
		}
		
		
		public synchronized Integer poll(){
			
			int currWorker = mutantWorkerQueue.poll();
			
			return currWorker;
		}
		
		public synchronized void add(Integer worker){
			mutantWorkerQueue.add(worker);
		}
		
		public synchronized void clear(){
			mutantWorkerQueue = new LinkedList<Integer>();
			
		}
		
		public synchronized boolean isEmpty(){
			return mutantWorkerQueue.isEmpty(); 
		}
		
		public synchronized int size(){
			return mutantWorkerQueue.size();
		}
	
	
}
