/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *******************************************************************************/
/**
 * Created by Kevin Moran on Mar 31, 2016
 */
package edu.semeru.android.mutation.pit;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.pitest.classinfo.ClassByteArraySource;
import org.pitest.classinfo.ClassName;

import org.pitest.functional.predicate.True;
import org.pitest.classpath.ClassloaderByteArraySource;
import org.pitest.mutationtest.engine.MutationDetails;
import org.pitest.mutationtest.engine.gregor.config.DefaultMutationEngineConfiguration;
import org.pitest.mutationtest.engine.Mutant;
import org.pitest.mutationtest.engine.Mutater;
import org.pitest.mutationtest.engine.gregor.GregorMutationEngine;
import org.pitest.mutationtest.engine.gregor.MethodInfo;
import org.pitest.mutationtest.engine.gregor.MethodMutatorFactory;
import org.pitest.mutationtest.engine.gregor.config.Mutator;
import org.pitest.mutationtest.engine.gregor.inlinedcode.InlinedFinallyBlockDetector;

import com.strobel.decompiler.Decompiler;
import com.strobel.decompiler.DecompilerSettings;
import com.strobel.decompiler.PlainTextOutput;


/**
 * This class was created (by Joel) as an example of how PIT's mutation engine can be used
 * directly to output mutations of a mutatee to file. Then, it was customized and extended by Kevin and Mario to
 * generate mutants from several files in a multi-threaded manner.
 * 
 * Original version of the file is here:  https://groups.google.com/forum/#!msg/pitusers/9MB4ZUhUJ34/SeECRxKqQo4J
 * 
 * @author Joel van den Berg
 * @author Kevin Moran
 * @author Mario Linares-Vasquez
 */
public class PITMutationWrapper {

    private ClassByteArraySource byteSource = null;
    public String targetDir;
    public Mutater mutater;
    public PITGenerationConfig config;
    URLClassLoader urlLoader = null;

    
    /**
     * Example of how to run the PIT wrapper on a set of .class files
     */
    public static void main(String[] args) throws ClassNotFoundException, Exception {
        PITGenerationConfig config = new PITGenerationConfig();
        
        //Output folder for the generated files
        config.setMutantsOutputFolder(args[0]);
        
        //This is the folder having the classes (i.e., .class files) for the main package
        config.setClassesFolder(args[1]);
        
        //This is required for those apps that require extras or specific jars from Android 
        config.setJarsFolder(args[2]);
        
        //Flag to control whether the .class files are decompiled to Java source code
        config.setDecompileClass(true);
        
        PITMutationWrapper wrapper = new PITMutationWrapper(config);
       // wrapper.generateMutants();
       // countMutantTypes(args[0]);
    }
    
    /**
     * Creates a PITMutate object with the default mutators. To change the
     * mutators another constructor can be made or the mutators collection can
     * be adapted. All possible mutators are in the {@link Mutator} class. If
     * the output directory doesn't yet exist, it will be created.
     *
 
     *
     * @throws Exception
     */
    public PITMutationWrapper(PITGenerationConfig config) throws Exception {
        int urlsToLoad = 1;
        List<File> temp = new ArrayList<>();
        List<File> libraries = new ArrayList<>();
        
        this.config = config;
        targetDir = this.config.getMutantsOutputFolder();

        if (targetDir.isEmpty()) {
            throw new Exception("Output directory can't be <empty>");
        }
        
        File idir = new File(this.config.getClassesFolder());
        
        if(this.config.getJarsFolder() != null){
            File jarsFolder = new File(this.config.getJarsFolder());
            if(jarsFolder.exists()){
               temp = (List<File>) FileUtils.listFiles(jarsFolder, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
                for (File library : temp) {
                    if(library.getName().endsWith(".jar")){
                        libraries.add(library);
                    }
                }
                urlsToLoad+=libraries.size();
        
            }
        }
        URL[] urls = new URL[urlsToLoad];
        urls[0] = new File(idir.getCanonicalPath()).toURI().toURL();
        int index = 1;
        for (File library : libraries) {
              urls[index] = library.toURI().toURL();
      
            index++;
        }
        urlLoader = URLClassLoader.newInstance(urls, this.getClass().getClassLoader());


        //ClassLoader loader = IsolationUtils.getContextClassLoader();

        byteSource = new ClassloaderByteArraySource(
                urlLoader);
        File odir = new File(targetDir);
        if (!odir.exists()) {
            odir.mkdir();
        }

      
        Collection<MethodMutatorFactory> mutators = Mutator.all();

        DefaultMutationEngineConfiguration engConfig = new DefaultMutationEngineConfiguration(True.<MethodInfo>all(), Collections.<String>emptyList(), mutators, new InlinedFinallyBlockDetector());
        GregorMutationEngine eng = new GregorMutationEngine(engConfig);

        //MutationConfig config = new MutationConfig(eng, Collections.<String>emptyList());
        //MutationConfig config = new MutationConfig(eng, Collections.<String>emptyList());
        mutater = eng.createMutator(byteSource);
    }

    /**
     * Mutate the class with the associated class name. The class needs to be on
     * the classpath before it can be mutated. Multiple mutants will be created
     * (depending on the mutators that are chosen) and returned in an ArrayList.
     * Each mutant in the ArrayList can then be written to file with
     * {@link PITMutate#writeMutantClassToFile(String, byte[], String)} by using
     * the {@link Mutant#getBytes()} method.
     *
     * @param className The name of the class that needs to be mutated.
     * @return An ArrayList of byte array that represent the mutant.
     */
    private ArrayList<Mutant> mutateClass(String className) throws Exception {

       
        Class<?> classToLoad = Class.forName(className, true, urlLoader);

        ClassName toBeMutated = new ClassName(classToLoad.getCanonicalName());
        Collection<MutationDetails> details = mutater.findMutations(toBeMutated);
        //Iterator<MutationDetails> iterator = details.iterator();
        ArrayList<Mutant> mutants = new ArrayList<Mutant>();
        for (MutationDetails detail : details) {

            Mutant mutant = mutater.getMutation(detail.getId());
            mutants.add(mutant);
        }

        return mutants;
    }

    /**
     * Writes a mutant's byte array to a file. The class name which contains the
     * package name will be used to create a folder structure in the target
     * directory. The dir_pre parameter is used to create a directory in the
     * target directory so that mutants with same class names can be written to
     * the target directory in different directories. E.g. a class with name
     * "foo.Bar", when dir_pre is defined as "pre" will end up in
     * "_targetDir_/pre/foo/Bar.class"
     *
     * @param className The name of the class that is written to file.
     * @param mutant The byte array representing the mutant
     * @param dirPre A directory that can be made to put the mutant in, which
     * makes it easier to write multiple mutants in the same directory
     * @throws FileNotFoundException
     * @throws IOException
     */
    private String writeMutantClassToFile(String className, byte[] mutant, String dirPre) throws FileNotFoundException, IOException {

        String[] dirs = className.split("\\.");
        String reldir = this.targetDir + File.separator + "bytecode";

        if (dirPre != null && dirPre != "") {
            File dir = new File(reldir, dirPre);
            dir.mkdir();
            reldir = dir.getAbsolutePath();
        }
        for (int i = 0; i < dirs.length - 1; i++) {
            reldir += File.separatorChar + dirs[i];
        }

        File dir = new File(reldir);
        dir.mkdirs();

        File file = new File(reldir, dirs[dirs.length - 1] + ".class");

        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fstream = new FileOutputStream(file);
        fstream.write(mutant);
        fstream.close();
        return file.getCanonicalPath();
    }

    public String generateMutants() throws IOException, MalformedURLException, ClassNotFoundException {

        System.out.println("*** Starting PIT mutants generation for classes at " + targetDir + " ***");
        String className = null;
        StringBuilder mutantsClasses = new StringBuilder();
        StringBuilder mutantsJava = new StringBuilder();
        String mutantClassList = "";
        String mutantSrcFile = "";
        String mutantList = "";

        MutationDetails details = null;
        File dir = new File(this.config.getClassesFolder());
        List<File> files = (List<File>) FileUtils.listFiles(dir, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
        BufferedWriter logWriter = new BufferedWriter(new FileWriter(targetDir + File.separator + "mutants.log"));
        String mutantPath = null;

        String srcFolder = targetDir + File.separator + "src";
        (new File(srcFolder)).mkdir();

        int mutantNumber = 1;
        for (File file : files) {
        	System.out.println(file.getCanonicalPath() + " " + this.config.getPackageName());
            if (file.getCanonicalPath().contains(this.config.getPackageName()) && file.getName().endsWith(".class") && !file.getName().contains("$") && !file.getName().contains("EmmaInstrumentation.class") && !file.getName().contains("FinishListener.class") && !file.getName().contains("InstrumentedActivity.class") && !file.getName().contains("SMSInstrumentedReceiver.class")) {
            	int dirPre = 0;
                System.out.println("\n-- Loading class " + file.getCanonicalPath() + "...");

                className = file.getCanonicalPath().replace(".class", "");
                className = className.replace(this.config.getClassesFolder(), "");
                className = className.replace(File.separator, "."); // to remove folder path (if any)
                if (className.startsWith(".")) {
                    className = className.substring(1);
                }
                System.out.println(className);

                System.out.print("-- PIT wrapper is generating mutants:...");

                ArrayList<Mutant> mutations = new ArrayList<>();
                try {
//                    mutations = mutateClass(className);
                	PITMutationWrapperThread mutationWorker = new PITMutationWrapperThread(className, this);
                	mutationWorker.start();
                	while(mutationWorker.isAlive()){
                		Thread.sleep(2000);
                	}
                	mutations = mutationWorker.getThreadMutations();
                	
                    System.out.println(" DONE");

                } catch (Exception ex) {
                    System.out.println("ERROR - " + ex);
                    
                }
                for (Mutant mutant : mutations) {
                    details = mutant.getDetails();

                    logWriter.write(mutantNumber + ";" + className + ";" + details.getMethod().name() + ";" + details.getLineNumber() + ";" + details.getMutator() + ";" + details.getDescription());
                    logWriter.newLine();
                     System.out.print("--- Writing mutant-" + mutantNumber + " to disk: ...");
                    try {
                        mutantPath = writeMutantClassToFile(className, mutant.getBytes(), "mutant-" + mutantNumber);
                        mutantsClasses.append(mutantPath);
                        mutantsClasses.append('\n');
                        System.out.println(" DONE");
                    } catch (Exception ex) {
                        System.out.println("ERROR - " + ex.getMessage());
                    }
                    if (config.isDecompileClass()) {
                        System.out.print("--- Decompiling class for mutant-" + mutantNumber + ": ....");
                        try {
                        	mutantSrcFile = srcFolder + File.separator + ("mutant-" + mutantNumber) + File.separator + className.replace(".", File.separator) + ".java";
                            decompileFile(mutantPath, mutantSrcFile);
                            mutantsJava.append(mutantPath);
                            mutantsJava.append('\n');
                            System.out.println(" DONE");
                        } catch (Exception ex) {
                            System.out.println("ERROR - " + ex.getMessage());
                        }

                    }
                    mutantNumber++;
                }
            }
        }
        logWriter.close();

        if(config.isDecompileClass()){
        	mutantList = mutantsJava.toString();
        }else{
        	mutantList = mutantClassList.toString();
        }
        return mutantList;
        
    }
   
    private void decompileFile(String filePath, String outputPath) throws FileNotFoundException, IOException {
        String dir = outputPath.substring(0, outputPath.lastIndexOf(File.separator));
        (new File(dir)).mkdirs();
        FileOutputStream fos = new FileOutputStream(outputPath);
        OutputStreamWriter osw = new OutputStreamWriter(fos);
        DecompilerSettings settings = DecompilerSettings.javaDefaults();
        Decompiler.decompile(filePath, new PlainTextOutput(osw), settings);

        osw.close();
        fos.close();

    }
    
}
