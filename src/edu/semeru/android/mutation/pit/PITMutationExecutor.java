/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *******************************************************************************/
/**
 * Created by Kevin Moran on Apr 4, 2016
 */
package edu.semeru.android.mutation.pit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import edu.semeru.android.mutation.helper.TerminalHelper;
import edu.semeru.android.mutation.service.ExtractDataService;

/**
 * @author KevinMoran
 *
 */
public class PITMutationExecutor {

	public static void main(String[] args) throws ClassNotFoundException, Exception {

		String apkFileList = args[0];
		String baseOutputFolder = args[1];
		String baseInputFolder = args[2];
		String libs4analysis = args[3];
		String baseProjectsFolder = args[4];
		String mutantProjectsFolder = args[5];
		String mutantAPKsFolder = args[6];
		String projectName = "";
		String packageName = "";

		PITGenerationConfig config = new PITGenerationConfig();

		System.out.println("--Starting Pit-For-Android Mutation Analysis Tool--");
		System.out.println();
		
		try (BufferedReader br = new BufferedReader(new FileReader(apkFileList))) {
			String currentFile;
			while ((currentFile = br.readLine()) != null) {

				projectName = currentFile.substring(currentFile.lastIndexOf("/") + 1, currentFile.indexOf(".apk"));
				System.out.println(projectName);

				System.out.println("-- Decompiling apk for " + projectName + " and applying Pit bytecode Mutations...");
				
				ExtractDataService.decompileAPKtoClasses(currentFile,
						baseInputFolder + File.separator + projectName + File.separator + projectName + ".jar ",
						baseInputFolder + File.separator + projectName + File.separator + "classes");
				ExtractDataService.decompileAPKtoDex(currentFile, baseProjectsFolder + File.separator + projectName);
				removeFile(baseProjectsFolder + File.separator + projectName + File.separator + "classes.dex");
				System.out.println();
				String getPackageCommand = "/Volumes/Macintosh_HD_3/AndroidSDK/sdk/build-tools/24.0.1/aapt dump badging " + currentFile;
//				String aaptOutput = "";
//				aaptOutput = TerminalHelper.executeCommand(getPackageCommand);
//				packageName = aaptOutput.substring(aaptOutput.indexOf("package name:")+16, aaptOutput.indexOf("versionCode")-2);
				packageName = ExtractDataService.extractPackage(baseProjectsFolder + File.separator + projectName + File.separator + "AndroidManifest.xml");
				
				packageName = packageName.replace(".", "/");
				System.out.println(packageName);
				
				config.setPackageName(packageName);
				
				// Output folder for the generated files
				config.setMutantsOutputFolder(baseOutputFolder + File.separator + projectName + File.separator);

				// This is the folder having the classes (i.e., .class files)
				// for the main package
				System.out.println(baseInputFolder + File.separator + projectName + File.separator + "classes" + File.separator + packageName + File.separator);
				config.setClassesFolder(
						baseInputFolder + File.separator + projectName + File.separator + "classes" + File.separator);

				// This is required for those apps that require extras or
				// specific jars from Android
				config.setJarsFolder(libs4analysis);

				// Flag to control whether the .class files are decompiled to
				// Java source code
				config.setDecompileClass(false);

				PITMutationWrapper wrapper = new PITMutationWrapper(config);
				wrapper.generateMutants();

						
				generateMutantAPKs(baseOutputFolder, baseInputFolder, projectName, mutantProjectsFolder,
						mutantAPKsFolder, baseProjectsFolder);

			} // End While loop to iterate through projects
		} // End try catch for Buffered/File Reader

	}

	public static void generateMutantAPKs(String baseOutputFolder, String baseInputFolder, String projectName,
			String mutantProjectsFolder, String mutantAPKsPath, String baseProjectsFolder) {
		
		int numOfThreads = 5;
		
		System.out.println();
		System.out.println("--- Generating Mutant APKs for " + projectName + "---");
		System.out.println();
		
		String countMutantsCommand = "cat " + baseOutputFolder + File.separator + projectName + File.separator
				+ "mutants.log | wc -l";
//		System.out.println(countMutantsCommand);

		String mutantCount = TerminalHelper.executeCommand(countMutantsCommand);

		System.out.println("-- " + mutantCount + " mutants will be assembled.");
		System.out.println();
		System.out.println("Assembly Process running on " + numOfThreads + " threads...");
		
		int muCount = Integer.parseInt(mutantCount);

		String copyCommand = "";
		String createDirCommand = "";
		String createDirCommand2 = "";
		String findCommand = "";
		String findResult = "";

		Path projectOutputPath = Paths.get(mutantAPKsPath + File.separator + projectName + File.separator);

		if (!Files.exists(projectOutputPath)) {

			createDirCommand = "mkdir " + mutantAPKsPath + File.separator + projectName + File.separator;

//			System.out.println(createDirCommand);
//			System.err.println(TerminalHelper.executeCommand(createDirCommand));
			TerminalHelper.executeCommand(createDirCommand);
		}

		MutantWorkerQueue workers = MutantWorkerQueue.getInstance();
		
		for (int i = 1; i <=5; i++){
			
			workers.add(i);
			
		}
		
		for (int i = 1; i <= muCount; i++) {

			int workerNum = workers.poll();
			
			PITMutationExecutorThread apkGenerator = new PITMutationExecutorThread(baseOutputFolder, baseInputFolder, projectName, mutantProjectsFolder, mutantAPKsPath, baseProjectsFolder, i, workerNum);
			
			apkGenerator.start();
			
			if(i==muCount/4){
				
				System.out.println("25% Complete...");
				
			}else if(i==muCount/2){
				
				System.out.println("50% Complete...");
				
			}else if(i == (muCount/4)*3){
				
				System.out.println("75% Complete...");
				
			}else if(i==muCount){
				
				System.out.println("100% Complete...");
				
			}
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			while(workers.isEmpty()){
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
//			System.out.println("-- Assembling Mutant #:" + i + "...");
//			
//			projectOutputPath = Paths.get(mutantProjectsFolder + File.separator + projectName + File.separator
//					+ "mutant-" + i + File.separator);
//
//			if (!Files.exists(projectOutputPath)) {
//
//				createDirCommand = "mkdir " + mutantProjectsFolder + File.separator + projectName + File.separator;
//				createDirCommand2 = "mkdir " + mutantProjectsFolder + File.separator + projectName + File.separator
//						+ "mutant-" + i + File.separator;
////				System.out.println(createDirCommand);
////				System.out.println(createDirCommand2);
////				System.err.println(TerminalHelper.executeCommand(createDirCommand));
////				System.err.println(TerminalHelper.executeCommand(createDirCommand2));
//				TerminalHelper.executeCommand(createDirCommand);
//				TerminalHelper.executeCommand(createDirCommand2);
//			}
//
//			copyCommand = "cp -R " + baseInputFolder + File.separator + projectName + File.separator + "classes/ "
//					+ mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + i
//					+ File.separator;
////			System.out.println(copyCommand);
////			System.err.println(TerminalHelper.executeCommand(copyCommand));
//			TerminalHelper.executeCommand(copyCommand);
//
//			findCommand = "find " + baseOutputFolder + File.separator + projectName + File.separator + "bytecode"
//					+ File.separator + "mutant-" + i + File.separator + " -name *.class";
//			findResult = TerminalHelper.executeCommand(findCommand);
//			
//			findResult = findResult.substring(findResult.lastIndexOf("/")+1, findResult.length());
//			
//			findCommand = "find " + mutantProjectsFolder + File.separator
//					+ projectName + File.separator + "mutant-" + i + File.separator + " -name " + findResult;
//			findResult = TerminalHelper.executeCommand(findCommand);
//			
//			removeFile(findResult);
//			
//			copyCommand = "cp -R " + baseOutputFolder + File.separator + projectName + File.separator + "bytecode"
//					+ File.separator + "mutant-" + i + File.separator + " " + mutantProjectsFolder + File.separator
//					+ projectName + File.separator + "mutant-" + i + File.separator;
////			System.out.println(copyCommand);
////			System.err.println(TerminalHelper.executeCommand(copyCommand));
//			TerminalHelper.executeCommand(copyCommand);
//
//			copyCommand = "cp -R " + baseProjectsFolder + File.separator + projectName + File.separator + " "
//					+ mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + i + "-project"
//					+ File.separator;
////			System.out.println(copyCommand);
////			System.err.println(TerminalHelper.executeCommand(copyCommand));
//			TerminalHelper.executeCommand(copyCommand);
//
//			ExtractDataService.compileClassesToJar(
//					mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + i
//							+ File.separator,
//					mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + i + ".jar ");
//			ExtractDataService.compileJarToDex(
//					mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + i + ".jar",
//					mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + i + ".dex");
//
//			copyCommand = "cp -R " + mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-"
//					+ i + ".dex " + mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + i
//					+ "-project" + File.separator + "classes.dex";
////			System.out.println(copyCommand);
////			System.out.println(TerminalHelper.executeCommand(copyCommand));
//			TerminalHelper.executeCommand(copyCommand);
//			ExtractDataService.compileDexToAPK(
//					mutantProjectsFolder + File.separator + projectName + File.separator + "mutant-" + i + "-project",
//					mutantAPKsPath + File.separator + projectName + File.separator + i + ".apk");
//			ExtractDataService
//					.signAPKwithDebugSig(mutantAPKsPath + File.separator + projectName + File.separator + i + ".apk");
//			System.out.println();

		}

	}

	public static void removeFile(String targetFile){
		
		String rmCommand = "rm " + targetFile;
		TerminalHelper.executeCommand(rmCommand);
	}
	
}
