/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *******************************************************************************/
/**
 * ExtractDataService.java
 * 
 * Created on Jun 20, 2014, 12:45:47 AM
 * 
 */
package edu.semeru.android.mutation.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import edu.semeru.android.mutation.helper.TerminalHelper;

/**
 * {Insert class description here}
 * 
 * @author Carlos Bernal & Kevin Moran
 * @since Jun 20, 2014
 */
public class ExtractDataService {

    
    public static void decompileAPKtoClasses(String pathToAPK, String jarOutputFile, String classesOutputPath) {

        System.out.println("-- Decompiling the APK into jar...");
        String jarOutputPath = jarOutputFile.substring(0,jarOutputFile.lastIndexOf("/"));
//      System.out.println(jarOutputPath);
        Path projectOutputPath = Paths.get(jarOutputPath);

        if (!Files.exists(projectOutputPath)) {

            String createDirCommand = "mkdir " + jarOutputPath;
//          System.out.println(createDirCommand);
//          System.err.println(TerminalHelper.executeCommand(createDirCommand));
            TerminalHelper.executeCommand(createDirCommand);

        }

        String decompileCommand = "libs/dex2jar/d2j-dex2jar.sh " + "-o " + jarOutputFile + " " + pathToAPK;
        System.out.println(decompileCommand);

        String decompileOutput = TerminalHelper.executeCommand(decompileCommand);
//      System.err.println(decompileOutput);

        System.out.println("-- Decompiling the jar into classes...");

        projectOutputPath = Paths.get(classesOutputPath);

        if (!Files.exists(projectOutputPath)) {

            String createDirCommand = "mkdir " + classesOutputPath;
//          System.out.println(createDirCommand);
//          System.err.println(TerminalHelper.executeCommand(createDirCommand));
            TerminalHelper.executeCommand(createDirCommand);
        }

        String unzipJarCommand = "unzip -o " + jarOutputFile + " -d " + classesOutputPath;
//      System.out.println(unzipJarCommand);
        String unzipOutput = TerminalHelper.executeCommand(unzipJarCommand);
//      System.err.println(unzipOutput);

    }

    public static void decompileAPKtoDex(String pathToAPK, String outputPath) {

        System.out.println("-- Using apktool to decompile the APK into dex + resources...");

        String decompileCommand = "libs/apktool d -s " + pathToAPK + " -o " + outputPath;
//      System.out.println(decompileCommand);

        String decompileOutput = TerminalHelper.executeCommand(decompileCommand);
//      System.err.println(decompileOutput);

    }

    public static void compileClassesToJar(String classesFolder, String outputJarFile) {

        //System.out.println("Compiling Class Files to Jar...");

        String compileCommand = "jar cf " + outputJarFile + " " + classesFolder;
//      System.out.println(compileCommand);

        String compileOutput = TerminalHelper.executeCommand(compileCommand);
//      System.err.println(compileOutput);

    }

    public static void compileJarToDex(String pathToJar, String outputFile) {

        //System.out.println("Compiling Jar File to Dex...");

        String compileCommand = "libs/dex2jar/d2j-jar2dex.sh " + pathToJar + " -o " + outputFile;
//      System.out.println(compileCommand);

        String compileOutput = TerminalHelper.executeCommand(compileCommand);
//      System.err.println(compileOutput);

    }

    public static void compileDexToAPK(String pathtoDexProject, String outputAPKPath) {

        //System.out.println("Compiling Dex Project to apk...");

        String compileCommand = "libs/apktool b " + pathtoDexProject + " -o " + outputAPKPath;
//      System.out.println(compileCommand);

        String compileOutput = TerminalHelper.executeCommand(compileCommand);
//      System.err.println(compileOutput);

    }

    public static void signAPKwithDebugSig(String pathToAPK) {

        //System.out.println("Signing APK with debug signature...");

        String signCommand = "jarsigner -verbose -keystore ~/.android/debug.keystore -storepass android -keypass android "
                + pathToAPK + " androiddebugkey";
//      System.out.println(signCommand);

        String signOutput = TerminalHelper.executeCommand(signCommand);
//      System.err.println(signOutput);

    }
    
    public static String extractPackage(String pathtoManifest) {

        SAXBuilder builder = new SAXBuilder();

        String manifest = pathtoManifest;
        //System.out.println(manifest);
        String packageName = "";

        Document document;
        try {
            document = (Document) builder.build(new File(manifest));
            Element rootNode = document.getRootElement();
                // System.out.println(rootNode);
                Namespace ns = Namespace.getNamespace("http://schemas.android.com/apk/res/android");
                Attribute children = rootNode.getAttribute("package");
                packageName = children.getValue();
                // processExpStmt(rootNode, pathOutput, line);
                // System.out.println();
        } catch (JDOMException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        

        return packageName;
    }
    
}