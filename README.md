# Pit Wrapper for Android

## Overview
This project adapts the [Pit Mutation tool](https://pitest.org) so that it can be applied to the bytecode of Android applications. This is a multi-threaded program that can be applied to generate thousands of mutants in a relatively efficient manner. 

## Use

The project dependencies are managed by Maven, and we provide the necessary files so that the project can be automatically imported into Eclipse. The use of the tool proceeds as follows:

1) Decompile the target Android application to be mutated using a combination of [APKTool]() and your Java Decompiler of Choice. There is also some supporting code for this in the `ExtractDataService` Class in the project.

2) Make sure all the .class files for the app are contained within a single folder which we will call the `ClassesFolder`.

3) Specify an empty folder where the mutants will be created called the `MutantsOutputFolder`.

4) If the app uses specific libraries that are packaged as jars, place all of these in a separate folder called the `JarsFolder`.

5) Run the main method in the `PITMutationWrapper` class passing in the absolute paths to the three folders above in the order they are introduced. The program should run and will generate mutants in the `MutantsOutputFolder`.

## Troubleshooting

If you have any issues or questions, please feel free to open a ticket and tag @kpmoran for assistance. 